﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.Data;

namespace Tracker.IRepository.Models
{
    public class ClockingModel
    {
        public int Id_Clocking { get; set; }
        public Nullable<int> Duration { get; set; }
        public System.DateTime StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public int User_Id { get; set; }
        public TaskModel Task { get; set; }
        public int? Task_Id { get; set; }
        public DateTime Date { get; set; }
    }
}
