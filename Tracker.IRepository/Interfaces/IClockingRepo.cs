﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.IRepository.Models;

namespace Tracker.IRepository.Interfaces
{
    public interface IClockingRepo
    {
        int AddClocking(ClockingModel clocking);
        ClockingModel GetClocking(int user_id, DateTime date);
        IEnumerable<ClockingModel> GetClockingList(int user_id, DateTime date);
        ClockingModel GetById(int id);
    }
}
