﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.IRepository.Interfaces;
using Tracker.IRepository.Models;
using Tracker.Services.Exceptions;
using Tracker.Services.IServices;
using Tracker.Services.Models;

namespace Tracker.Services.Services
{
    public class ClockInService : IClockInService
    {
        private IClockingRepo ClockingRepo;
        private ITaskRepository TaskRepo;

        public ClockInService(IClockingRepo clockingRepo, ITaskRepository taskRepo)
        {
            this.ClockingRepo = clockingRepo;
            this.TaskRepo = taskRepo;
        }

        public List<ClockingModel> GetAllClockingsForToday(int user_id)
        {
            var currDate = DateTime.Now;
            var lst = ClockingRepo.GetClockingList(user_id, currDate).ToList();
            lst.ForEach(x =>
            {
                if (x.Task_Id != null)
                {
                    var task = TaskRepo.GetById((int)x.Task_Id);
                    x.Task = task;
                }
            });
            return lst;

        }

        public void ClockIn(int user_id)
        {
            var currDate = DateTime.Now;
            var clocked = ClockingRepo.GetClocking(user_id, currDate);
            if (clocked != null && !clocked.EndTime.HasValue)
                throw new BaseException("Cannot clock in, clock out first");
            ClockingRepo.AddClocking(new ClockingModel()
            {
                // future add task Id
                StartTime = currDate,
                User_Id = user_id
            });
        }

        public void ClockOut(int user_id)
        {
            var currDateTime = DateTime.Now;
            var clocked = ClockingRepo.GetClocking(user_id, currDateTime);
            if (clocked == null) throw new BaseException("Needs Clock In");
            if (clocked.EndTime.HasValue) throw new BaseException("Already Clocked Out");
            //update end time
            if (clocked != null && !clocked.EndTime.HasValue)
            {
                clocked.EndTime = currDateTime;
            }
            ClockingRepo.AddClocking(clocked);
        }

        public ClockingInfoDTO GetClockingStatusForDay(DateTime date, int user_id)
        {
            var clockingInfo = new ClockingInfoDTO();
            var clocking = ClockingRepo.GetClocking(user_id, date);
            if (clocking == null) clockingInfo.NeedsClockIn = true; // false -> needs clock in
            else
            {
                if (clocking.EndTime.HasValue)
                {
                    clockingInfo.NeedsClockIn = true; // false -> entry exists, new clock in
                }
                else
                {
                    clockingInfo.NeedsClockIn = false;
                }
            }
           

            // duration in hours
            var duration = ClockingRepo.GetClockingList(user_id, date)
                .Where(x => x.EndTime.HasValue)
                .Sum(x => x.EndTime.Value.Hour - x.StartTime.Hour);

            clockingInfo.TotalClocked = duration;
            //clockingInfo.LeftTimeForAllocation = duration - timeEntrySum;

            return clockingInfo;
        }

        public int AssignTask(int task_id, int clocking_id)
        {
            var clocking = ClockingRepo.GetById(clocking_id);
            if (clocking == null)
            {
                throw new BaseException("Clocking not found with Id " + clocking_id);
            }

            var task = TaskRepo.GetById(task_id);
            if (task == null)
            {
                throw new BaseException("Task not found with Id " + task_id);
            }

            clocking.Task_Id = task_id;

            return ClockingRepo.AddClocking(clocking);

            
        }
    }
}
