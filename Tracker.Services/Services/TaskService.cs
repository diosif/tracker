﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tracker.IRepository.Interfaces;
using Tracker.IRepository.Models;
using Tracker.Services.Exceptions;
using Tracker.Services.IServices;

namespace Tracker.Services.Services
{
    public class TaskService : ITaskService
    {
        private ITaskRepository TaskRepo;
        private ITaskGroupRepository TaskGroupRepo;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public TaskService(ITaskRepository taskRepo, ITaskGroupRepository taskGroupRepo)
        {
            TaskRepo = taskRepo;
            TaskGroupRepo = taskGroupRepo;
        }

        public List<TaskModel> GetTasks(TaskFilter filter)
        {
            var tasks = TaskRepo.GetAllTasks(filter).ToList();
            tasks.ForEach(x =>
            {
                if (x.TaskGroup_Id != null)
                {
                    getAndSaveTaskGroupToModel(x);
                }
            });
            return tasks;
        }

        public int UpsertTask(TaskModel task)
        {
            if (task == null)
                {
                    throw new BaseException("TaskService: UpsertTask did not receive a Task!");
                }
            if (string.IsNullOrEmpty(task.Name))
                throw new BaseException("Invalid Task name");

            return TaskRepo.AddTask(task);
        }

        public TaskModel GetById(int id)
        {
            if (id == null)
            {
                throw new BaseException("TaskService: GetById did not receive an id");
            }
            return TaskRepo.GetById(id);
        }

        private void getAndSaveTaskGroupToModel(TaskModel t)
        {
            var taskGroup = TaskGroupRepo.GetById((int)t.TaskGroup_Id);
            t.TaskGroup = taskGroup;
        }
    }
}
