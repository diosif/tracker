﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracker.Services.Models
{
    public class TaskClockingDTO
    {
        public int task_id { get; set; }
        public int clocking_id { get; set; }
    }
}
