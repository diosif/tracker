﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracker.Services.Models
{
    public class ClockingInfoDTO
    {
       public bool NeedsClockIn { get; set; }
        public decimal TotalClocked { get; set; }
        public decimal LeftTimeForAllocation { get; set; }
    }
}
