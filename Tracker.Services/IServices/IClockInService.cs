﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.IRepository.Models;
using Tracker.Services.Models;

namespace Tracker.Services.IServices
{
    public interface IClockInService
    {
        void ClockIn(int user_id);
        void ClockOut(int user_id);
        ClockingInfoDTO GetClockingStatusForDay(DateTime date, int user_id);
        List<ClockingModel> GetAllClockingsForToday(int user_id);
        int AssignTask(int task_id, int clocking_id);


    }
}
