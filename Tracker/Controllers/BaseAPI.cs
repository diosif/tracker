﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;

namespace Tracker.Controllers
{
    public class BaseAPI : ApiController
    {
        public int UserID
        {
            get
            {
                var identity = User as ClaimsPrincipal;
                var value = identity.Claims.FirstOrDefault(x => x.Type == "sub").Value;
                return Convert.ToInt32(value);
            }
        }
    }
}
