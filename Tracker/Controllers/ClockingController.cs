﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tracker.IRepository.Models;
using Tracker.Services.Exceptions;
using Tracker.Services.IServices;
using Tracker.Services.Models;
using Tracker.Services.Services;

namespace Tracker.Controllers
{
    [Authorize]
    public class ClockingsController : BaseAPI
    {
        protected IClockInService ClockInService;

        public ClockingsController(ClockInService clockinService)
        {
            ClockInService = clockinService;
        }

        [HttpGet]
        public IHttpActionResult ClockIn(int user_id)
        {
            var currDate = DateTime.Now;
            var info = ClockInService.GetClockingStatusForDay(currDate, user_id);

            //if(UserID gasesc AllClocking un entry pentru date == currDate)
            if (info.NeedsClockIn)
            {
                ClockInService.ClockIn(user_id);
            }

            return Ok();
        }

        [HttpGet]
        public IHttpActionResult ClockOut(int user_id)
        {
            var currDate = DateTime.Now;
            var info = ClockInService.GetClockingStatusForDay(currDate, user_id);

            //if(UserID gasesc AllClocking un entry pentru date == currDate)
            if (!info.NeedsClockIn)
            {
                ClockInService.ClockOut(user_id);
            }
           
            return Ok();
        }


        [HttpGet]
        public IHttpActionResult GetClockingStatusByUser(int user_id)
        {
            var curentDate = DateTime.Now;

            return Ok(ClockInService.GetClockingStatusForDay(curentDate, user_id )); 
        }

        [HttpGet]
        public IHttpActionResult GetClockingsForToday(int user_id)
        {
            return Ok(ClockInService.GetAllClockingsForToday(user_id));
        }

        [HttpGet]
        public IHttpActionResult GetUserId()
        {
            return Ok(UserID);
        }

        [HttpPost]
        public IHttpActionResult AssignTaskToClocking([FromBody] TaskClockingDTO DTO)
        {
            return Ok(ClockInService.AssignTask(DTO.task_id, DTO.clocking_id));
        }

    }



    public class ClockIn
    {
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
