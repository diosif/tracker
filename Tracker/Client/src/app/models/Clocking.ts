import { TaskModel } from "./TaskModel";

export class ClockingModel {
  Id_Clocking: number;
  Duration: number;
  StartTime: Date;
  EndTime: Date;
  User_Id: number;
  Task: TaskModel;
  Task_Id: number;
  Date: Date;
}
