export class ClockingInfo {
  NeedsClockIn: boolean;
  TotalClocked: number;
  LeftTimeForAllocation: number;
} 
