import { TaskGroup } from "./TaskGroup";

export class TaskModel{
    Id_Task: number;
    Name: string;
    Description: string;
    TaskGroup_Id: number;
    TaskGroup: TaskGroup;
}

export class TaskFilter {
  Name: string;
}
