import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
 import { ToasterService } from 'angular2-toaster';
import { ClockingModel } from '../../models/Clocking';
import { ClockinService } from '../../services/clockingService';
import { TaskModel, TaskFilter } from '../../models/TaskModel';
import { TaskService } from '../../services/taskService';
import { TaskClocking } from '../../models/TaskClocking';
import { timestamp } from 'rxjs/operator/timestamp';

@Component({
  selector: 'clocking-cmp',
  templateUrl: './clocking.component.html',
  styleUrls: ['./clocking.component.css']
})
export class ClockingComponent implements OnInit {
  clockins: Array<ClockingModel>;
  tasks: Array<TaskModel>
  taskClockin: TaskClocking = new TaskClocking();
  user_Id: number;
  clockIn: boolean;
  lastClockIn: Date;


  constructor(private router: Router, private clockinService: ClockinService,
    private taskService: TaskService,
    private toaster: ToasterService) {}

  ngOnInit(): void {
    this.getUserId();
    this.getTasks();
  }

  clickClockIn() {
    this.toggleButton();
    this.clockinService.clockIn(this.user_Id);
    //this.clockinService.getClockingsForToday(this.user_Id);
  }

  clickClockOut() {
    this.toggleButton();
    this.clockinService.clockOut(this.user_Id);
    this.getClockings();
  }

  getTasks() {
    this.taskService.getAllTasks(new TaskFilter()).then(
      tasks => this.tasks = tasks,
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }

  assignTask(id_clocking:number, id_task: number) {
    this.taskClockin.clocking_id = id_clocking;
    this.taskClockin.task_id = id_task;
    this.clockinService.assignTaskToClocking(this.taskClockin).then(
      rsp => {
        this.toaster.pop('success', 'Task assigned');
        this.getClockings();
      },
      error => {
        this.toaster.pop('error', error.error);
      }
    )
  }


  toggleButton() {
    this.clockIn == true ? this.clockIn = false : this.clockIn = true;
  }

  getClockingStatus() {
    this.clockinService.getClockinStatus(this.user_Id).then(
      rsp => {
        this.clockIn = rsp.NeedsClockIn
      },
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }

  getUserId() {
    this.clockinService.getUserId().then(
      id => {
        this.user_Id = id;
        this.getClockings();
        this.getClockingStatus();
      },
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }

  getClockings() {
    this.clockinService.getClockingsForToday(this.user_Id).then(
      clockins => this.clockins = clockins,
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }




}
