import { Component, OnInit } from '@angular/core';
import { TaskModel, TaskFilter } from '../../models/TaskModel';
import { TaskService } from '../../services/taskService';
import { Router } from '../../../../node_modules/@angular/router';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  filter: TaskFilter = new TaskFilter();
  tasks: Array<TaskModel>;
  task: TaskModel;
  errorMessage: string;

  constructor(private router: Router, private taskService: TaskService,
              private toaster: ToasterService) { }

  ngOnInit() {
    this.getTasks();
    this.task = new TaskModel();
  }

  insertTask() {
    this.router.navigateByUrl('tasks/upsert');
  }

  saveTask(item: TaskModel){
    if (item){
      this.router.navigateByUrl('tasks/upsert/' + item.Id_Task);
    } else {
      this.router.navigateByUrl('tasks/upsert');
    }
  }
  
  getTasks() {
    this.taskService.getAllTasks(this.filter).then(
        tasks => this.tasks = tasks,
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }

  

}


