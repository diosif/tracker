import { TaskService } from "../../../services/taskService";
import { TaskModel } from "../../../models/TaskModel";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "../../../../../node_modules/@angular/router";
import { ToasterService } from 'angular2-toaster';
import { TaskGroup, TaskGroup_Filter } from "../../../models/TaskGroup";
import { TaskGroupService } from "../../../services/taskGroupService";

@Component({
  selector: 'app-tasks',
  templateUrl: './task-upsert.component.html'
})
export class TaskUpsertComponent implements OnInit {
  task: TaskModel;
  id_task: number;
  taskGroups: TaskGroup[];
  tgFilter: TaskGroup_Filter;

  constructor(private router: Router,
    private taskService: TaskService,
    private route: ActivatedRoute,
    private toaster: ToasterService,
    private taskGroupService: TaskGroupService) {

    this.route.params.subscribe(params => {
      this.id_task = params['id'];
      if (this.id_task > 0)
        this.taskService.getById(this.id_task).then(rsp => {
          this.task = rsp;
        }, err => {
          this.toaster.pop('error', err.error)
        });
    });
  }

  ngOnInit() {
    this.task = new TaskModel();
    this.tgFilter = new TaskGroup_Filter();
    this.taskGroupService.getAllTaskGroups(this.tgFilter).then(
      success => {
        this.taskGroups = success;
      },
      error => {
        this.toaster.pop('error', error.error);
      });
  }

  goBack() {
    this.router.navigateByUrl('/tasks');
  }

  addTask() {
    if (!this.task.Name || !this.task.Description) {
      this.toaster.pop("error", "Cannot add task", "Task Name or Description is empty");
      return;
    }

    console.log(this.task);
    this.taskService.saveTask(this.task).then(
      success => {
        this.toaster.pop('success', this.id_task > 0 ? 'Task edited' : 'Task added');
        this.goBack();
      },
      error => {
        this.toaster.pop('error', error.error);
      }
    );
  }

  selectTaskGroup(taskGroup_Id: number) {
    this.task.TaskGroup_Id = taskGroup_Id;
  }

}

