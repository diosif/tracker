import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { TaskModel, TaskFilter } from '../models/TaskModel';


@Injectable()
export class TaskService {
    constructor(private http: HttpClient){}

    getAllTasks(filter: TaskFilter) {
        return this.http.get<TaskModel[]>('/api/tasks/GetTasks?filter=' + JSON.stringify(filter)).toPromise();
    }

  getById(task_Id: number) {
    return this.http.get<TaskModel>('api/tasks/GetById?id=' + task_Id).toPromise();
  }

    saveTask(task: TaskModel) {
        return this.http.post<TaskModel>('/api/tasks/UpsertTask', task).toPromise();
    }

}
