import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ClockingInfo } from "../models/ClockingInfo";
import { ClockingModel } from "../models/Clocking";
import { TaskClocking } from "../models/TaskClocking";

@Injectable()
export class ClockinService {
  constructor(private http: HttpClient) { }

  getClockinStatus(user_id: number) {
    return this.http.get<ClockingInfo>('api/clockings/GetClockingStatusByUser?user_id=' + user_id).toPromise();
  }

  getClockingsForToday(user_id: number) {
    return this.http.get<ClockingModel[]>('api/clockings/GetClockingsForToday?user_id=' + user_id).toPromise();
  }

  assignTaskToClocking(taskClocking: TaskClocking) {
    return this.http.post<TaskClocking>('/api/clockings/AssignTaskToClocking', taskClocking).toPromise();
  }

  getUserId() {
    return this.http.get<number>('/api/clockings/GetUserId').toPromise();
  }

  clockIn(user_id: number) {
    return this.http.get('api/clockings/ClockIn?user_id=' + user_id).toPromise();
  }

  clockOut(user_id: number) {
    return this.http.get('api/clockings/ClockOut?user_id=' + user_id).toPromise();
  }

}
