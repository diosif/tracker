﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.IRepository.Interfaces;
using Tracker.IRepository.Models;
using Tracker.Data;

namespace Tracker.Repository.Repository
{
    public class TaskRepository: BaseRepository, ITaskRepository
    {
        public int AddTask(TaskModel task)
        {
            var dbT = DbContext.Tasks.Where(x => x.Id_Task == task.Id_Task).FirstOrDefault();
            var add = false;
            if (dbT == null)
            {
                dbT = new Tasks();
                add = true;
            }

            dbT.Name = task.Name;
            dbT.Description = task.Description;
            dbT.TaskGroup_Id = task.TaskGroup_Id;

            if (add == true) {
                DbContext.Tasks.Add(dbT);
            }

            return DbContext.SaveChanges();
        }
        
        public IQueryable<TaskModel> GetTask_QRY()
        {
            var result = DbContext.Tasks.Select(x => new TaskModel()
            {
                Name = x.Name,
                Description = x.Description,
                TaskGroup_Id = x.TaskGroup_Id,
                Id_Task = x.Id_Task,
            }).AsQueryable();

            return result;
        }

        public IEnumerable<TaskModel> GetAllTasks(TaskFilter filter)
        {
            bool filterByName = false;
            if (!string.IsNullOrEmpty(filter.Name))
                filterByName = true;
            return GetTask_QRY().Where(x =>
            (filterByName ? x.Name.Contains(filter.Name) : true)
            ).ToList();
        }

        public TaskModel GetById(int id)
        {
            return GetTask_QRY().Where(x => x.Id_Task == id).FirstOrDefault();
        }

        public bool UpdateTask(TaskModel task)
        {
            var result = DbContext.Tasks.Find(task.Id_Task);
            if (result == null) return false;
            
            else
            {
                if (task.Name != null) result.Name = task.Name;
                if (task.Description != null) result.Description = task.Description;
                if (task.Project_Id!= null) result.Description = task.Description;
                if (task.TaskGroup_Id != null) result.TaskGroup_Id= task.TaskGroup_Id;
                DbContext.SaveChanges();
                return true;
            }
        }
    }
}
