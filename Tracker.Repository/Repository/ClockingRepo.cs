﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracker.IRepository.Interfaces;
using Tracker.IRepository.Models;
using Tracker.Data;

namespace Tracker.Repository.Repository
{
    public class ClockingRepo : BaseRepository, IClockingRepo
    {
        public IQueryable<ClockingModel> GetClocking_QRY()
        {
            return DbContext.Clocking.Select(x => new ClockingModel()
            {
                Duration = x.Duration,
                EndTime = x.EndTime,
                Id_Clocking = x.Id_Clocking,
                StartTime = x.StartTime,
                User_Id = x.User_Id, 
                Task_Id = x.Task_Id,
            }).AsQueryable();
        }

        public ClockingModel GetClocking(int user_id, DateTime date)
        {
            //return GetClocking_QRY().Where(x=>
            // DateTime.Compare(x.Date, date) == 0).FirstOrDefault();
            var all = GetClocking_QRY().ToList();
            return all.Where(x => x.StartTime.DayOfYear == date.DayOfYear).OrderByDescending(x => x.StartTime).FirstOrDefault();
                
        }

        public IEnumerable<ClockingModel> GetClockingList(int user_id, DateTime date)
        {
            //DbFunctions.CreateDateTi
            var all = GetClocking_QRY().Where(x => x.User_Id == user_id).ToList();
            return all.Where(x=> x.StartTime.DayOfYear == date.DayOfYear).ToList();
        }

        public ClockingModel GetById(int id)
        {
            return GetClocking_QRY().Where(x => x.Id_Clocking == id).FirstOrDefault();
        }

        // Upsert
        public int AddClocking(ClockingModel clocking)
        {
            var clk = DbContext.Clocking.Where(x => x.Id_Clocking == clocking.Id_Clocking).FirstOrDefault();
            var add = false;

            if (clk == null)
            {
                clk = new Clocking();
                add = true;
            }

            clk.StartTime = clocking.StartTime;
            clk.EndTime = clocking.EndTime;
            if (clocking.EndTime.HasValue)
            {
                clk.Duration = clk.EndTime.Value.Subtract(clk.StartTime).Minutes;
            }
            clk.User_Id = clocking.User_Id;
            if (clk.Task_Id == null)
            {
                clk.Task_Id = clocking.Task_Id;
            }

            if (add)
            {
                DbContext.Clocking.Add(clk);
            }

            return DbContext.SaveChanges();
        }

    }
}
